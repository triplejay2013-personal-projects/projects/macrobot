"""Loads macro config and executes."""
from pathlib import Path
import asyncio
import pickle
from typing import Callable, Tuple
import mouse
import keyboard
from constants import ClickType

class Bot:
    def __init__(self, configfile: str, watcher: object = None) -> None:
        self.watcher = watcher
        self.shutdown = asyncio.Event()
        with open(Path(__file__).parent / configfile, "rb") as f:
            self.config = pickle.load(f)

    async def _mouse_macro(self, macro):
        elapsed, x, y, click_type = macro.keys()
    
        click = {
            ClickType.LEFT: mouse.LEFT,
            ClickType.MIDDLE: mouse.MIDDLE,
            ClickType.RIGHT: mouse.RIGHT,
        }.get(macro[click_type])
        await asyncio.sleep(macro[elapsed])
        mouse.move(macro[x], macro[y])
        mouse.click(click)

    async def _keyboard_macro(self, macro):
        elapsed, key = macro.keys()
        await asyncio.sleep(macro[elapsed])
        keyboard.press_and_release(f"shift+{macro[key]}" if macro[key].isupper() else macro[key])

    async def _run_macro(self):
        for event in self.config:
            if self.shutdown.is_set() or self.watcher.kill_event.is_set():
                break
            while any([f.is_set() for f in self.watcher.event_flags]):
                # Wait for subscribed watcher events to clear
                await asyncio.sleep(0)
            macro = event.get("mouse")
            if macro:
                await self._mouse_macro(macro)
            else:
                macro = event.get("keyboard")
                if macro:
                    await self._keyboard_macro(macro)
                else:
                    macro = event.get("elapsed")
                    if macro:
                        await asyncio.sleep(macro)
            print(macro)
        self.shutdown.set()

    async def run(self):
        loop = asyncio.get_running_loop()
        macro_task = loop.create_task(self._run_macro())

        print("Press Ctrl-C to quit.")
        while not self.shutdown.is_set():
            if keyboard.is_pressed("ctrl+c"):
                self.shutdown.set()
                await asyncio.sleep(0)

                if not macro_task.done():
                    macro_task.cancel()
            await asyncio.sleep(0)