"""Monitors and records coordinate configurations."""
from pathlib import Path
import pickle
import asyncio
import time
from typing import Callable, Optional, Tuple
import mouse
import keyboard
from constants import ClickType

class Config:
    """Holds macro configuration.
    
    Attributes:
        shutdown (Event): Signals shutdown
        recording (Event): Signals we are recording
        macro (List): holds coordinates when the key is pressed
        events (Dict): Keeps track of events that happen
        configfile (str): Where we will save config when we are done
        curr_event (str): The last registered event
        elapsed_time (float): The time we started recording between each macro
    """

    def __init__(self, configfile: str, watcher: object = None) -> None:
        self.watcher = watcher  # TODO how to respond to subscription events?
        self.shutdown = asyncio.Event()
        self.recording = asyncio.Event()
        self.macro = []
        self.events = {
            ClickType.LEFT: 0,
            ClickType.MIDDLE: 0,
            ClickType.RIGHT: 0,
        }
        self.configfile = Path(__file__).parent / configfile
        self.curr_event = None
        self.elapsed_time = 0

        def events_update_callback(event):
            self.events[event] += 1
            self.curr_event = event

        # Events tracking (for terminal printout)
        mouse.on_click(events_update_callback, (ClickType.LEFT,))
        mouse.on_middle_click(events_update_callback, (ClickType.MIDDLE,))
        mouse.on_right_click(events_update_callback, (ClickType.RIGHT,))

        def save_stroke(key: keyboard.KeyboardEvent):
            # If we aren't recording, or if any events have triggered, don't save the macro. This prevents saving macro actions to our record
            if self.recording.is_set() and not any([f.is_set() for f in self.watcher.event_flags]):
                # TODO - this is a workaround for the start record key
                if key.name in ["shift", "r", "R"]:
                    return
                macro = {"keyboard": {"elapsed": time.time() - self.elapsed_time, "key": key.name}}
                print(f"Saving macro: {macro}")
                self.macro.append(macro)
                self.elapsed_time = time.time()

        def save_coordinates(click_type: ClickType):
            # If we aren't recording, or if any events have triggered, don't save the macro. This prevents saving macro actions to our record
            if self.recording.is_set() and not any([f.is_set() for f in self.watcher.event_flags]):
                x, y = mouse.get_position()
                macro = {"mouse": {"elapsed": time.time() - self.elapsed_time, "x": x, "y": y, "type": click_type}}
                print(f"Saving macro: {macro}")
                self.macro.append(macro)
                self.elapsed_time = time.time()

        mouse.on_click(save_coordinates, (ClickType.LEFT,))
        keyboard.on_release_key("R", self.toggle_record)
        keyboard.on_release(save_stroke)

    def toggle_record(self, *args):
        if self.recording.is_set():
            self.recording.clear()
            # If any time was waited at the end, we will also wait
            self.macro.append({"elapsed": time.time() - self.elapsed_time})
            with open(self.configfile, "wb") as f:
                pickle.dump(self.macro, f)
        else:
            print("\n")
            self.recording.set()
            self.elapsed_time = time.time()

    @staticmethod
    def exists(configfile: str) -> bool:
        return (Path(__file__).parent / configfile).exists()

    async def learn(self):
        """Print current position and wait for events.
        
        Notes:
            This method expects nothing else to print to auto-update the position.
        """
        print("Press Ctrl-C to quit, and Shift+r to start/stop macro recording.")
        while not self.shutdown.is_set():
            if keyboard.is_pressed('ctrl+c'):
                self.shutdown.set()
                break
            else:
                # Surrender event loop to other tasks first
                await asyncio.sleep(0.01)

            if self.recording.is_set():
                await asyncio.sleep(0)
            else:
                # Wait until we are done recording
                x, y = mouse.get_position()
                position = f"X: {str(x).rjust(6)} Y: {str(y).rjust(6)} Event: {str(self.curr_event).rjust(15)} Count: {str(self.events).rjust(30)}"
                print(position, end='')
                print("\b" * len(position), end='\r', flush=True)
        print("\n")