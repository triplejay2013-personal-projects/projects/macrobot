"""Detect on screen events and allow users to subscribe to those events."""
from typing import Callable
import asyncio
import pyautogui as pag
import os
from pathlib import Path
from functools import partial


class EventWatcher:
    """Register images to look for."""

    class Config:
        CALLBACK_DELAY = 0.01

    def __init__(self, plugin: str) -> None:
        # Load resources
        resources_path = Path(__file__).parent / plugin
        if not resources_path.exists():
            raise ValueError(f"{resources_path} does not exist")
        self.resources = [resources_path / resource for resource in os.listdir(resources_path)]

        self.event_flags = list()
        self.callbacks = dict()
        self.shutdown = asyncio.Event()
        # If something happens, and we want to signal other services to stop watching/acting
        self.kill_event = asyncio.Event()

    def find(self, to_find: str) -> str:
        """Find full path to requested resource image."""
        for f in self.resources:
            if f.stem == to_find or f.name == to_find:
                return f.__str__()

    def register(self, name: str, handler: Callable):
        """Register an event with a handler.
        
        Args:
            name (str): Name of on-screen event we want to look for.
            handler (Callable): What to do if the onscreen event occurs
        """
        event = partial(pag.locateCenterOnScreen, self.find(name), grayscale=True, confidence=0.7)
        flag = asyncio.Event()
        self.callbacks[name] = (event, handler, flag)
        self.event_flags.append(flag)
        return flag  # Return to caller if they care about when this triggers

    def unregister(self, name: str):
        """Unregister event by name."""
        if self.callbacks.get(name):
            print(f"Unregistering {name}")
            del self.callbacks[name]

    async def watch(self):
        """Watch for events, and handle them accordingly."""
        print(f"Loaded {len(self.resources)} events")
        print(f"Watching {len(self.callbacks)} registered callbacks")
        while not self.shutdown.is_set():
            for key, val in self.callbacks.items():
                event, handler, flag = val
                if event():
                    print(f"{key} triggered. Handling...")
                    handler()
                    flag.set()
                # Give up event loop for a moment
                await asyncio.sleep(self.Config.CALLBACK_DELAY)
            # Reset event flags
            for flag in self.event_flags:
                flag.clear()
            await asyncio.sleep(0)


if __name__ == "__main__":
    watcher = EventWatcher("btd6")
    def handler1():
        x, y = pag.locateCenterOnScreen(watcher.find("ok"), grayscale=True, confidence=0.7)
        pag.click(x, y)

    def handler2():
        res = pag.locateCenterOnScreen(watcher.find("restart"), grayscale=True, confidence=0.7)
        pag.click(*res)
        import time
        time.sleep(3)
        res = pag.locateCenterOnScreen(watcher.find("restart_confirm"), grayscale=True, confidence=0.7)
        pag.click(*res)

    async def test1(a, b):
        while not a.is_set():
            print("foo")
            await asyncio.sleep(0)
        while not b.is_set():
            print("bar")
            await asyncio.sleep(0)

    async def test2(a, b):
        a.set()
        b.set()

    listener1 = watcher.register("overwrite_save", handler1)
    listener2 = watcher.register("game_over", handler2)

    loop = asyncio.new_event_loop()
    try:
        loop.create_task(test1(listener1, listener2))
        loop.create_task(test2(listener1, listener2))
        loop.run_until_complete(watcher.watch())
    except KeyboardInterrupt:
        pass
    finally:
        _, _, flag = watcher.callbacks["overwrite_save"]
        print(watcher.event_flags[0] == flag)
        print(watcher.event_flags[1] == flag)

        watcher.unregister("overwrite_save")
        watcher.unregister("game_over")