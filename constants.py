class ClickType:
    LEFT = "LeftClick"
    MIDDLE = "MiddleClick"
    RIGHT = "RightClick"

class BTD6Hotkeys:
    # Upgrade hotkeys (after tower is selected)
    UPGRADE_1 = "q"
    UPGRADE_2 = "w"
    UPGRADE_3 = "e"

    DART = "d"
    HERO = "h"