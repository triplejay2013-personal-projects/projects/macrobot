import asyncio
from bot import Bot
from config import Config
from event_watcher import EventWatcher
import pyautogui as pag

# TODO build this file into a Tkinter application for easy use?
# TODO package as windows executable

DEFAULT_CONFIGFILE = "coordinate_config.pickle"

class EmergencyGameBreak(Exception):
    """Raise if something happens to end the game immediately."""

async def play_game(configfile: str, watcher):
    bot = Bot(configfile, watcher)
    counter = 0
    try:
        while True:
            counter += 1
            print(f"Starting game #{counter}")
            await bot.run()
            yn = input("Play again [y/n]? ")
            if not yn or (yn and yn[0].lower() not in ["y"]):
                break
            bot.shutdown.clear()
            watcher.kill_event.clear()
    finally:
        print(f"Played {counter} times. Thank you for playing")

async def walkthrough(configfile: str, watcher):
    config = Config(configfile, watcher)
    loop = asyncio.get_running_loop()
    await asyncio.wait([
        loop.create_task(config.learn()),
    ])


def game_over_handler(watcher):
    pag.click(pag.locateCenterOnScreen(watcher.find("home"), grayscale=True, confidence=0.7))
    watcher.kill_event.set()


if __name__ == "__main__":
    loop = asyncio.new_event_loop()
    configfile = input("Enter configfile name (leave empty for default): ")
    plugin = input("Enter name of plugin (leave empty if none): ") or None
    bot_watcher = None
    if plugin:
        bot_watcher = EventWatcher(plugin)
        if plugin == "btd6":
            bot_watcher.register("overwrite_save", lambda: pag.click(pag.locateCenterOnScreen(bot_watcher.find("ok"), grayscale=True, confidence=0.7)))
            bot_watcher.register("game_over", lambda: game_over_handler(bot_watcher))
    if not configfile:
        configfile = DEFAULT_CONFIGFILE
    configfile = f"macros/{configfile}"
    if bot_watcher:
        bot_watcher_task = loop.create_task(bot_watcher.watch())
    try:
        if Config.exists(configfile):
            loop.run_until_complete(play_game(configfile, bot_watcher))
        else:
            loop.run_until_complete(walkthrough(configfile, bot_watcher))
    finally:
        if bot_watcher:
            bot_watcher.shutdown.set()
        loop.run_until_complete(asyncio.wait([bot_watcher_task], timeout=5))